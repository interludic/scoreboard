@extends('admin.layouts.admin')

@section('header')
	<title>Admin Area | Games</title>

	<style type="text/css">

	</style>

</head>
<body class="skin-blue sidebar-collapse" ng-app="hqMatch" ng-controller="MainController" ng-init="init()">
@endsection

@section('content')		
	<!-- Main content -->
	
	<!-- <div ng-init="game = {!! $game !!}"></div>
	<div ng-init="players = {!! $players !!}"></div> -->

	<div ng-init="game = {&quot;id&quot;:52,&quot;name&quot;:&quot;5 on 5 - 1 hour indoor (Loose Cannons)&quot;,&quot;description&quot;:&quot;&lt;p&gt;&lt;b&gt;Each week we play\u00a0a friendly competitive game of basketball. Once the ball has been tossed, your only break is the occasional 4 minute substitution during a non stop hour. Maximise your fitness each game while we pump some tunes!\u00a0&lt;\/b&gt;&lt;\/p&gt; &lt;p&gt;Players of various skills and gender play at the same time. Experienced players lead their team against equally matched opponents. Teams are designated and randomised at the start of the game by the event organiser.\u00a0&lt;\/p&gt; &lt;p&gt;Points are totalled and the team with the most wins.\u00a0&lt;\/p&gt; &lt;p&gt;Some of our members are casual, while many are regular, this creates a dynamic mix of friends and new faces, this\u00a0&lt;a href=\&quot;http:\/\/www.basketball613.com.au\/code-of-conduct\/\&quot;&gt;culture&lt;\/a&gt;\u00a0is what separates Basketball613 apart from standard league competition basketball.\u00a0&lt;\/p&gt; &lt;p&gt;The rules are refereed by the players, for the players. We use the FIBA\u00a0rule system.&lt;\/p&gt; &lt;p&gt;Coloured sashes\u00a0are provided to players to identify which team you'r on, so you just need to bring comfortable clothes.&lt;\/p&gt; &lt;p&gt;Beginners and new comers are welcome,&lt;a href=\&quot;http:\/\/www.basketball613.com.au\/help\/groups-skill-levels\/\&quot;&gt;\u00a0click here for details about finding out which group you are in&lt;\/a&gt;\u00a0before you attend. When you arrive ask reception where the basketball courts are.&lt;\/p&gt; &lt;p&gt;&lt;b&gt;Payment is made online, preferably 1 week in advance.&lt;\/b&gt;&lt;\/p&gt; &lt;p&gt;Facilities: Showers, Parking, Trains, Drinking Water.\u00a0&lt;\/p&gt;&quot;,&quot;status&quot;:&quot;upcoming&quot;,&quot;time&quot;:&quot;1458630000000&quot;,&quot;response_limit&quot;:&quot;14&quot;,&quot;fee_amount&quot;:14,&quot;featured&quot;:0,&quot;venue_id&quot;:&quot;1412922&quot;,&quot;visibility&quot;:&quot;public_limited&quot;,&quot;venue_visibility&quot;:&quot;0&quot;,&quot;meetup_event_id&quot;:&quot;zmvcnlyvfbdc&quot;,&quot;photo_url&quot;:&quot;\/&quot;,&quot;guest_limit&quot;:&quot;&quot;,&quot;created_at&quot;:&quot;2015-12-24 20:47:13&quot;,&quot;updated_at&quot;:&quot;2015-12-24 20:47:13&quot;}"></div>
	<div ng-init="players = [{&quot;name&quot;:&quot;Organiser [M]&quot;,&quot;meetup_id&quot;:&quot;197056409&quot;,&quot;user_id&quot;:&quot;&quot;,&quot;photo&quot;:&quot;http:\/\/photos1.meetupstatic.com\/photos\/member\/c\/a\/a\/3\/thumb_252231875.jpeg&quot;,&quot;status&quot;:&quot;yes&quot;,&quot;guests&quot;:0}]"></div>
	
		
	<!-- Player list:
	<ul>
		<li ng-repeat="player in players"><input ng-model="player.teamId" type="checkbox"> <% player.userId %> <% player.userName %></li>
	</ul> -->
		
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				🏀<% game.name %> <button ng-click="lockPlayersList = toggle(lockPlayersList)" class="btn btn-flat btn-primary pull-right"><i class="fa fa-lock"></i> Players List</button>				
				
				<!-- TODO show diff live! -->

			</div>
		</div>

		<div class="row">
			<!-- LIGHT TEAM -->
			<div ng-class="lockPlayersList ? 'col-md-4' : 'col-md-6'">
			<!-- //FIXME pass the colour.teamLight into a function that checks the colour array for the colourClass, eg: bg-black -->
				<div class="box box-default <% getColourClass(colour.teamLight) %>">
					<div class="box-header with-border">
			  		  <h3 class="box-title <% getColourClass(colour.teamLight) %>">Light Team</h3> 
			  		  <input type="number" name="teamLight" ng-model="scores.teamLight" size="3" class="pull-right" ng-minlength="0" min="0" max="999" ng-maxlength="999">
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<h1 ng-class="lockPlayersList ? 'scoreSmall' : 'scoreLarge'" ng-model="scores.teamLight"><% scores.teamLight %></h1>				
					</div>
					<!-- /.box-body -->
					<div class="box-footer <% getColourClass(colour.teamLight) %>">
						<button ng-click="scores.teamLight = plusScore(scores.teamLight, 2)" class="btn btn-flat btn-default btn-lg"><i class="fa fa-plus"></i>2</button>
						<button ng-click="scores.teamLight = plusScore(scores.teamLight, 3)" class="btn btn-flat btn-default btn-lg"><i class="fa fa-plus"></i>3</button>
						<button ng-click="scores.teamLight = minusScore(scores.teamLight)" class="btn btn-flat btn-default btn-lg pull-right"><i class="fa fa-minus"></i></button>						
		      </div>
				</div>

			</div>

			<!-- Center Column VS & player list  -->
			<div class="col-md-4" ng-class="lockPlayersList ? '' : 'hidden'">
				<h1 style="font-size: 8vw; text-align:center;"><br/><br/>VS</h1>
			</div>


			<!-- DARK TEAM -->			
			<div ng-class="lockPlayersList ? 'col-md-4' : 'col-md-6'">
				<div class="box box-default <% getColourClass(colour.teamDark) %>">
					<div class="box-header with-border">
			  		  <h3 class="box-title <% getColourClass(colour.teamDark) %>">Dark Team</h3> <input type="number" name="teamDark" ng-model="scores.teamDark" size="3" class="pull-right" ng-minlength="0" min="0" max="999" ng-maxlength="999">
					</div>
					<!-- /.box-header -->
					<div class="box-body <% getColourClass(colour.teamDark) %>">
						<h1 ng-class="lockPlayersList ? 'scoreSmall' : 'scoreLarge'" ng-model="scores.teamDark" ><% scores.teamDark %></h1>				
					</div>
					<!-- /.box-body -->
					<div class="box-footer <% getColourClass(colour.teamDark) %>">
						<button ng-click="scores.teamDark = plusScore(scores.teamDark, 2)" class="btn btn-flat btn-default btn-lg"><i class="fa fa-plus"></i>2</button>
						<button ng-click="scores.teamDark = plusScore(scores.teamDark, 3)" class="btn btn-flat btn-default btn-lg"><i class="fa fa-plus"></i>3</button>
						<button ng-click="scores.teamDark = minusScore(scores.teamDark)" class="btn btn-flat btn-default btn-lg pull-right"><i class="fa fa-minus"></i></button>
					</div>
				</div>				
			</div>
		</div> <!-- ./Row -->

		<!-- Players list -->
 		<div class="row" ng-class="lockPlayersList ? '' : 'hidden'">

			<div ng-class="lockPlayersList ? 'col-md-4' : 'col-md-6'">
				<div ui-sortable="sortableOptions" class="apps-container" ng-model="list1">
				  <div class="btn btn-flat <% getColourClass(colour.teamLight) %>" style="width:100%;" ng-repeat="app in list1"><% app.name %></div>
				</div>
		    <br />
				<select name="teamLightColour" id="colourSelect" class="form-control" ng-init="colours" ng-model="colour.teamLight" ng-change="saveDetails()">
					<option ng-repeat="colour in colours.availableOptions" value="<% colour.label %>" ><% colour.label %></option>
				</select>
			</div>

			<div class="col-md-4" >
				<div ui-sortable="sortableOptions" class="apps-container " ng-model="list2">
		      <div class="btn btn-flat btn-default" style="width:100%;" ng-repeat="app in list2"><% app.name %></div>
		    </div>
			</div>

			<div ng-class="lockPlayersList ? 'col-md-4' : 'col-md-6'">
		  	<div ui-sortable="sortableOptions" class="apps-container " ng-model="list3">
		      <div class="btn btn-flat <% getColourClass(colour.teamDark) %>" style="width:100%;" ng-repeat="app in list3"><% app.name %></div>
		    </div>
		    <br />
				<select name="teamDarkColour" id="colourSelect" class="form-control" ng-init="colours" ng-model="colour.teamDark" ng-change="saveDetails()">
					<option ng-repeat="colour in colours.availableOptions" value="<% colour.label %>" ><% colour.label %></option>
				</select>
			</div>		
		</div> <!-- ./Row -->

		<div class="row">
			<div class="col-md-12">				
				<h4>
					<div ng-show="postSuccess" class="label label-success">
						<% statusBody %>
					</div>
					<div ng-show="postFail" class="label label-danger">
						<% statusTitle %><br/><% statusBody %>
					</div>
				</h4>

				<h4>
					<button class="btn btn-primary btn-flat" ng-click="postMatchResults()"> Finish Game </button>
					<button ng-click="clearGameStorage()" class="btn btn-flat btn-warning"><i class="fa fa-trash"></i> Clear this game</button>					
					<button ng-click="clearStorage()" class="btn btn-flat btn-danger pull-right"><i class="fa fa-trash"></i> Clear ALL Storage</button></h4>

				<h4><small><strong>Event:</strong><% game.id %> <strong>Status:</strong><% game.status %> <strong><a href="http://meetup.com/Basketball613/events/<% game.meetup_event_id %>" target="_blank">Meetup:</a></strong><% game.meetup_event_id %> <!-- <% game.description %> <% game.venue_id %> --> </small>
					<span ng-show="localStorageStatus">Local Storage Ready</span><span ng-if="!localStorageStatus" class="label label-danger">Local Storage Unavailable</span></h4>
				<!-- <hr />
				<div class="debug">
					<p class="text-info"><% players | json %></p>
				 	<button type="button" ng-click="logModels()">Log Models</button>
				    <ul class="list logList">
				      <li ng-repeat="entry in sortingLog" class="logItem">
				        <% entry %>
				      </li>
				    </ul>			 
				</div> -->
			</div>
		</div> <!-- ./Row -->

	</section><!-- /.content -->

@endsection
	
@section('footer')
	@include('partials.ngmatch')
@endsection