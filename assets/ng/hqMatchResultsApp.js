//TODO play audio file each time player scores (nba jam inspiration) [pending: load times must be low as on 3G in the field]
var matchApp = angular.module('hqMatch', ['ngSanitize', 'ui.sortable', 'LocalStorageModule', 'oitozero.ngSweetAlert'], function($interpolateProvider) {
  $interpolateProvider.startSymbol('<%');
  $interpolateProvider.endSymbol('%>');
});

matchApp.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('hqMatchResults')
    .setNotify(true, true)
});

matchApp.controller('MainController', function($scope, localStorageService, $http, SweetAlert){
  $scope.lockPlayersList = true;
  // $scope.game = [];
  $scope.scores = [];
  $scope.playersList = [];
  $scope.colour = { 
    teamLight: { colour : "Light", colourClass : "bg-white"}, 
    teamDark: { colour : "Dark", colourClass : "bg-black" }
  };

  //INIT players rawList for UI
  $scope.rawList = [
    [], //Light Team
    [   /* { name : "User Name 1", userId : 100 }, */], //Players list
    []  //Dark Team
  ];

  init = function  () {
    if (localStorageService.get("players")===null) {
      // $scope.players = [
      //  { taskName : "Create an Angular-js TodoList" , isDone : false }
      // ];
      // $scope.players = localStorageService.get("players");
      console.log('Local Storage is live');
    }else{
      console.log('Local Storage is null');
    }

    var storageType = localStorageService.getStorageType();
    console.log('Storage type: '+storageType);
  };
  
  $scope.$watch($scope.game, function(){

    (getItem('teamLightColour'+$scope.game.id)) ? $scope.colour.teamLight = JSON.parse(getItem('teamLightColour'+$scope.game.id)) : $scope.colour.teamLight = 'Light';
    (getItem('teamDarkColour'+$scope.game.id)) ? $scope.colour.teamDark = JSON.parse(getItem('teamDarkColour'+$scope.game.id)) : $scope.colour.teamDark = 'Dark';    

    (getItem('teamLightScore'+$scope.game.id)) ? $scope.scores.teamLight = JSON.parse(getItem('teamLightScore'+$scope.game.id)) : $scope.scores.teamLight=0;  
    (getItem('teamDarkScore'+$scope.game.id)) ? $scope.scores.teamDark = JSON.parse(getItem('teamDarkScore'+$scope.game.id)) : $scope.scores.teamDark=0;    

    (getItem('teamLight'+$scope.game.id)) ? $scope.list1 = JSON.parse(getItem('teamLight'+$scope.game.id)) : $scope.list1 = $scope.rawList[0];
    (getItem('initPlayersList'+$scope.game.id)) ? $scope.list2 = JSON.parse(getItem('initPlayersList'+$scope.game.id)) : $scope.list2 = $scope.rawList[1];
    (getItem('teamDark'+$scope.game.id)) ? $scope.list3 = JSON.parse(getItem('teamDark'+$scope.game.id)) : $scope.list3 = $scope.rawList[2];
    
    // (getItem('teamLight'+$scope.game.id)) ? console.log("LS team light "+getItem('teamLight'+$scope.game.id)) : '';
    // (getItem('initPlayersList'+$scope.game.id)) ? console.log("LS initPlayersList"+getItem('initPlayersList'+$scope.game.id)) : '';
    // (getItem('teamDark'+$scope.game.id)) ? console.log("LS team dark"+getItem('teamDark'+$scope.game.id)) : '';
  });
  
  // console.log(" --- ");
  //TODO get scores from db?
  // console.log("INIT? from db");
  // console.log("init Light Team : "+$scope.list1);  
  // console.log("init Players List : "+$scope.list2);
  // console.log("init Dark Team : "+$scope.list3);
  // console.log(" --- ");

  ////////////////////////////////////////////////
  //Sortable OPTIONS
  $scope.sortableOptions = {
    placeholder: "app",
    connectWith: ".apps-container",
    // containment : "parent",//Dont let the user drag outside the parent
    cursor : "move",//Change the cursor icon on drag
    tolerance : "intersect",//Read http://api.jqueryui.com/sortable/#option-tolerance
    update: function(event, ui) {
      // on cross list sortings recieved is not true
      // during the first update
      // which is fired on the source sortable
      if (ui.item.sortable.received) {
        //TODO firing the move event! Now
        // console.dir(ui.item);
        // console.log(" ui.item = "+ui.item+"  userid "+ui.item.userId+" list?");
        $scope.savePlayerList();
      }
    }
  };

  //INIT scores
    // console.log($scope.game.id);
    // console.log(game.id);
  
  //players from the DB
  $scope.$watch($scope.players, function(){
    angular.forEach($scope.players, function(value, key) {
        console.log("player: "+value.name+" u="+value.user_id+" m="+value.meetup_id);
        $scope.rawList[1].push({ name : value.name, userId : value.user_id});  
    });
  });


  $scope.savePlayerList = function(){
    // $scope.playersList = [];

    // for (var i = 0; i < $scope.rawList.length; i++) {
    //   $scope.playersList[i] = $scope.rawList[i].map(function (x) {
    //     return {name : x.name, userId : x.userId};
    //   });
    // }

    // console.log("$scope.playersList count = "+$scope.playersList.length);
    console.log("Saving players...");
    localStorageService.set('teamLight'+$scope.game.id, angular.toJson($scope.list1));
    localStorageService.set('initPlayersList'+$scope.game.id, angular.toJson($scope.list2));
    localStorageService.set('teamDark'+$scope.game.id, angular.toJson($scope.list3));

    (getItem('teamLight'+$scope.game.id)) ? console.log("save - team light "+getItem('teamLight'+$scope.game.id)) : '';
    (getItem('initPlayersList'+$scope.game.id)) ? console.log("save - initPlayersList"+getItem('initPlayersList'+$scope.game.id)) : '';
    (getItem('teamDark'+$scope.game.id)) ? console.log("save - team dark"+getItem('teamDark'+$scope.game.id)) : '';
    
    // console.dir($scope.rawList[1]);
  };

  
  $scope.logModels = function () {
    $scope.savePlayerList();
    // $scope.sortingLog = [];
    // for (var i = 0; i < $scope.rawList.length; i++) {
    //   var logEntry = $scope.rawList[i].map(function (x) {
    //     return x.userId;
    //   }).join(', ');
      
    //   // logEntry = 'container ' + (i+1) + ': ' + logEntry;
    //   logEntry = ['list'+(i+1), logEntry];
      
    //   $scope.sortingLog.push(logEntry);
    // }

    // clearAll();
    // localStorageService.set('players', angular.toJson($scope.sortingLog));
    // console.log("item stored : "+getItem('players'));
    
  };


  ////////////////////////////
  //Scores & Colours
  $scope.$watch('scores.teamLight', function(){
    saveScores();    
  });

  $scope.$watch('scores.teamDark', function(){
    saveScores();    
  });

  $scope.$watch('colour.teamLight', function(){
    saveDetails();    
  });
  
  $scope.$watch('colour.teamDark', function(){
    saveDetails();    
  });

  function saveDetails(){
    localStorageService.set('teamLightColour'+$scope.game.id, angular.toJson($scope.colour.teamLight));
    localStorageService.set('teamDarkColour'+$scope.game.id, angular.toJson($scope.colour.teamDark));
    console.log(" ---localStorageService Details Saved dark = "+$scope.colour.teamDark+" light = "+$scope.colour.teamLight);
  };

  function saveScores(){
    // $scope.scoresArray = [];
    // $scope.scoresArray.push(['teamLight'+$scope.game.id, $scope.scores.teamLight]);
    // $scope.scoresArray.push(['teamDark'+$scope.game.id, $scope.scores.teamDark]);
    // // $scope.scoresArray.push($scope.scores);
    // //TODO scores live update
    // localStorageService.set('scores', angular.toJson($scope.scoresArray));
    // console.log("scores Stored : "+getItem('scores'));
    localStorageService.set('teamLightScore'+$scope.game.id, angular.toJson($scope.scores.teamLight));
    localStorageService.set('teamDarkScore'+$scope.game.id, angular.toJson($scope.scores.teamDark));
    console.log(" --- Scores Saved");
    console.log("Team light "+getItem('teamLightScore'+$scope.game.id)+" Team dark "+getItem('teamDarkScore'+$scope.game.id));
  };

  //TODO show history of scores.. with time interval (seconds ago..)
  $scope.plusScore = function(score, amt){
    if(score < 999){
      score = score + amt;      
    }
    return score;
  };

  $scope.minusScore = function(score){
    if(score > 0){
      score = score - 1;  
    }
    return score;
  };

  $scope.colours = {
    colourSelect: null,
    availableOptions: [
      { label : "Blue", colourClass: "bg-aqua" },
      { label : "Green", colourClass: "bg-green active" },
      { label : "Yellow", colourClass: "bg-yellow" },
      { label : "Red", colourClass: "bg-red active" },
      { label : "Purple", colourClass: "bg-purple" },
      { label : "Orange", colourClass: "bg-orange active" },
      { label : "Pink", colourClass: "bg-maroon" },
      { label : "Dark", colourClass: "bg-black active" },
      { label : "Light", colourClass: "bg-white" }
    ],
  };

  $scope.getColourClass = function(pick){
    var colourClass = pick;
    $scope.colours.availableOptions.forEach(function(colour) {
        if(pick == colour.label){
          colourClass = colour.colourClass;          
        }
    });
    return colourClass;
  }


  ////////////////////////////
  //LocalStorage
  
  $scope.localStorageStatus = localStorageService.isSupported;
    if(!localStorageService.isSupported) {
      SweetAlert.swal({
       title: "Local Storage is not supported",
       text: "Use chrome browser please!",
       type: "warning",
       showCancelButton: true,
    });
  }  
  
  // $scope.$watch("players",function  (newVal,oldVal) {
  //   // localStorageService.set('players', angular.toJson(newVal));
  //   // console.log("item stored : "+getItem('players'));
  
  //  // if (newVal !== null && angular.isDefined(newVal) && newVal!==oldVal) {
  //  //  localStorageService.add("players",angular.toJson(newVal));
  //  // }
  // },true);

  function getItem(key) {
   return localStorageService.get(key);
  }

  $scope.clearStorage = function(){
    SweetAlert.swal({
      title: "Are you sure?",
      text: "This will destroy ALL games on this device. Do this if you need to see updated players.",
      type: "warning",
      showCancelButton: true,
      closeOnCancel: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",      
      closeOnConfirm: false}, 
    function(isConfirm){ 
      if (isConfirm) {
        clearAll();
        window.location.reload();
        SweetAlert.swal({
          title: "Done!",
          timer: 500,
        });
      }
    });
  }

  function clearAll() {
   return localStorageService.clearAll();
  }

  $scope.clearGameStorage = function(){
    SweetAlert.swal({
      title: "Are you sure?",
      text: "This will destroy the data stored for this game only.",
      type: "warning",
      showCancelButton: true,
      closeOnCancel: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",      
      closeOnConfirm: false}, 
    function(isConfirm){ 
      if (isConfirm) {
        clearGame();
        window.location.reload();
        SweetAlert.swal({
          title: "Done!",
          timer: 500,
        });
      }
    });
  }

  function clearGame() {
   return localStorageService.remove('teamLightScore'+$scope.game.id, 'teamDarkScore'+$scope.game.id, 'teamLight'+$scope.game.id, 'teamDark'+$scope.game.id, 'details'+$scope.game.id);
  }


  ///////////////////////////////////
  // POST DATA
  //TODO CSRF issue..
  //TODO refactor? plenty of code dupe.
  //Refactor wip move scores into details.
  // check out match results
  $scope.postMatchResults = function () {     
      // global data
      var data = {
          teamLightScore: getItem('teamLightScore'+$scope.game.id),
          teamDarkScore: getItem('teamDarkScore'+$scope.game.id),
          teamLightColour: getItem('teamLightColour'+$scope.game.id),
          teamDarkColour: getItem('teamDarkColour'+$scope.game.id),
          teamLight: getItem('teamLight'+$scope.game.id),
          teamDark: getItem('teamDark'+$scope.game.id), 
          eventId: $scope.game.id,
          charset: "utf-8",
          _token: $('meta[name="csrf-token"]').attr('content')
      };    

      //--Redirect to route----------------------------
        // var form = $('<form/></form>');
        // form.attr("action", "/admin/games");
        // form.attr("method", "POST");
        // form.attr("style", "display:none;");
        // this.addFormFields(form, data);        
        // $("body").append(form);
        // form.submit();
      //------------------------------

      $http({
        method: 'POST',
        url: '/admin/games',
        data: data,
        // headers: { 'Content-Type': 'application/x-www-form-urlencoded' },

      }).then(function successCallback(response) {
          // this callback will be called asynchronously
          // when the response is available
          $scope.postSuccess = true;
          $scope.statusBody = response.data.message;
          SweetAlert.swal("Game Results:", response.data.message, "success");

        }, function errorCallback(response) {
          // Check connection?

          // called asynchronously if an error occurs
          // or server returns response with an error status.
          $scope.postFail = true;
          $scope.statusTitle = response.status;
          // $scope.statusBody = response.data;
          SweetAlert.swal(response.status, "Try saving again! Check internet?", "error");

        });

        // item data
        // for (var i = 0; i < this.items.length; i++) {
        //     var item = this.items[i];
        //     var ctr = i + 1;
        //     data["item_number_" + ctr] = item.sku;
        //     data["item_name_" + ctr] = item.name;
        //     data["quantity_" + ctr] = item.quantity;
        //     data["amount_" + ctr] = item.price.toFixed(2);
        // }
  }


  ////////////////////////////
  //Helpers
  $scope.toggle = function(item){
    return (item) ? false : true;
  };

  $scope.addFormFields = function (form, data) {
    if (data != null) {
        $.each(data, function (name, value) {
            if (value != null) {
                var input = $("<input></input>").attr("type", "hidden").attr("name", name).val(value);
                form.append(input);
            }
        });
    }
  }

  $scope.toNumber = function (value) {
      value = value * 1;
      return isNaN(value) ? 0 : value;
  }

}) //End Main Controller